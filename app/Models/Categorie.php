<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;

    public function plats() {
        return $this->hasMany(Plat::class);
    }

    public function plat_favori(){
        $platPlusCommande = null;
        $maxQuantite = 0;

        foreach ($this->plats as $plat) {
            $quantite = $plat->commandes->sum('pivot.nombre');
            if ($quantite > $maxQuantite) {
                $maxQuantite = $quantite;
                $platPlusCommande = $plat;
            }
}
 return $platPlusCommande;
    }
}
