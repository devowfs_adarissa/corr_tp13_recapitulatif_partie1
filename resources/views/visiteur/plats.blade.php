@extends("templates.visiteur")
@section("titre", "Plats")
@section("contenu")
@isset($categorie)
    <div class="card text-bg-dark col-12 mb-4">
        <img src="{{ asset($categorie->photo) }}" class="card-img" alt="..." >
        <div class="card-img-overlay">
            <h5 class="card-title">{{ $categorie->titre }}</h5>
            <p class="card-text">{{ $categorie->description }}</p>
            <p class="card-text"><small> {{$categorie->plats->count()}} plats</small></p>
        </div>
    </div>
@endisset
@isset($plats)
    @foreach($plats as $plat)

    <div class="card mb-3" >
  <div class="row no-gutters">
    <div class="col-md-3">
         @isset($plat->photo)
                <img src="{{ asset($plat->photo) }}" class="card-img" width="80%" />
            @else
                <img src="{{ asset('plats/default.png') }}" class="card-img"  style="object-fit:none" />
            @endisset
    </div>
    <div class="col-md-6">
      <div class="card-body">
        <h5 class="card-title" >{{ $plat->intitule }}</h5>
        <h5 class="card-title text-primary" >{{ $plat->prix }} Dhs</h5>
       <p class="card-text"><b>Decsription : </b> {{$plat->description}} <br>
                <b>Compositions : </b>
                @foreach($plat->composants as $composant)
                    {{$composant->libelle}}, 
                @endforeach 
                ...
            </p>      
        </div>
    </div>
       <div class="col-md-3">
        <p>
             <?php

  $nombreCommande=$plat->commandes->sum(function($cmd){
           return $cmd->pivot->nombre;
             });
             ?>
        
            @switch(true)
                @case($nombreCommande>=100 )
                    <i class="bi bi-star-fill" style="color:gold"></i>    
                @case($nombreCommande>=50 && $nombreCommande<100)
                    <i class="bi bi-star-fill" style="color:gold"></i>    
                @case($nombreCommande>=20 && $nombreCommande<50)
                    <i class="bi bi-star-fill" style="color:gold"></i>    
                @case($nombreCommande>=4 )
                    <i class="bi bi-star-fill" style="color:gold"></i>    
                @default
                    <i class="bi bi-star-fill" style="color:gold"></i>            
            @endswitch 
               <small> ({{$nombreCommande}})</small>
        </p>
       </div>
  </div>
</div>
 
    @endforeach
@endisset
@endsection